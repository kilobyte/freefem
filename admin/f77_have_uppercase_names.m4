dnl -*- Mode: m4 -*-
dnl
dnl SUMMARY:      $Id: f77_have_uppercase_names.m4,v 1.3 2000/06/13 22:25:33 prudhomm Exp $
dnl
dnl AUTHOR:       Christophe Prud'homme
dnl ORG:          Christophe Prud'homme
dnl E-MAIL:       Christophe.Prudhomme@ann.jussieu.fr
dnl
dnl ORIG-DATE:    26-Dec-98 at 17:50:54
dnl
dnl DESCRIPTION:  
dnl This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl  
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl DESCRIP-END.


dnl F77_UPPERCASE_NAMES()
AC_DEFUN(F77_HAVE_UPPERCASE_NAMES,
[AC_MSG_CHECKING([whether $F77 uses uppercase external names])
AC_CACHE_VAL(sl_cv_f77_uppercase_names,
[sl_cv_f77_uppercase_names=no
cat > conftest.f <<EOF
      subroutine xxyyzz ()
      return
      end
EOF
if ${F77-f77} -c conftest.f 1>&AC_FD_CC 2>&AC_FD_CC; then
  if test "`${NM-nm} conftest.o | grep XXYYZZ`" != ""; then
    sl_cv_f77_uppercase_names=yes
  fi
fi])
AC_MSG_RESULT([$sl_cv_f77_uppercase_names])
if test "$sl_cv_f77_uppercase_names" = yes; then
  AC_DEFINE(F77_UPPERCASE_NAMES, 1)
fi])
