// Emacs will be in -*- Mode: c++ -*-
//
// ********** DO NOT REMOVE THIS BANNER **********
//
// SUMMARY: Language for a Finite Element Method

//
// AUTHORS:  C. Prud'homme
// ORG    :          
// E-MAIL :  prudhomm@users.sourceforge.net
//
// ORIG-DATE:     June-94
// LAST-MOD:     13-Aug-00 at 23:19:18 by Christophe Prud'homme
//
// DESCRIPTION:  
/*
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
// DESCRIP-END.
//

#ifndef __FEM1C_H
#define __FEM1C_H 1

namespace fem
{
  void rhsPDE(int quadra, creal*  fw, creal*  f, creal*  g);
       
  float gaussband (creal*  a, creal*  x, long n, long bdthl, int first, float eps);

  float pdeian(creal*  a, creal*  u, creal*  f, creal*  g, creal*  u0, 
	       creal*  alpha, creal*  rho11, creal*  rho12, creal*  rho21, creal*  rho22,
	       creal*  u1, creal*  u2, creal*  beta, int quadra, int factorize);

}
#endif /* __FEM1C_H */
