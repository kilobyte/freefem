/* -*- Mode: pde -*- */

border(1,0,2*pi,60){
x := 5*cos(t);
y := 5*sin(t);
};

border(2,0,14,57){
if(t<=1)then  { x := 2-t; y := -3};
if((1<t)and(t<=7))then {x:=1; y:=-4+t};
 if((7<t)and(t<=8))then {x:=t-6; y:=3}
else  if(8<t) then {x:=2; y:=11-t}
};

border(3,0,14,57){
if(t<=1)then  { x := -1-t; y := -3};
if((1<t)and(t<=7))then {x:=-2; y:=-4+t};
 if((7<t)and(t<=8))then {x:=t-9; y:=3}
else  if(8<t) then {x:=-1; y:=11-t}
};

buildmesh(800);
solve(v){
onbdy(1) v = 0;
onbdy(2) v = 1;
onbdy(3) v = -1;
pde(v) -laplace(v) =0;
};
plot(v);
