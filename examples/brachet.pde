changewait;
border(1,0,1,20) begin x:=t; y:=0; end;
border(1,0,1,20) begin x:=1; y:=t; end;
border(1,0,1,20) begin x:=1-t; y:=1; end;
border(1,0,1,20) begin x:=0; y:=1-t; end;

border(2,0,2*pi,30)
 begin x:=0.5+0.15*cos(-t); y:=0.5+0.15*sin(-t); end;

buildmesh(1500);

i:=1;  dt:=0.05; z=1.2;
iter(10) begin
g = Re(z)^2+Im(z)^2; g=0;
solve(u,i) begin
onbdy(1,2) u=0;
pde(z) 
/*   id(u)*(1-dt + g*dt)
   -laplace(z)*(1+0*I)*dt 
   = z+(Re(z)-I*Im(z))*dt;
*/
id(u)-laplace(u)*dt=1.5*z;
end;
z=2*u-z;
i:=-1; t:=z(0.2,0); /*save('point.dta',t);*/
plot(z);
end;
